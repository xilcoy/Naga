/*
    Naga configuration tools to support Razer's Naga Mouse

    Copyright (C) 2015 Coy Barnes

    This code is very heavily derived from the Nostromo_n50 configuration tools
    for Belkin's Nostromo n50
    Copyright (C) 2003 Paul Bohme and others

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "ui_support.h"
#include "ui.h"

#include <sys/types.h>
#include <pwd.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>

#include "naga_data.h"
#include <FL/Fl_Choice.H>
#include <FL/Fl.H>
#include <FL/x.H>

/// Dynamically managed list of configurations
Fl_Menu_Item* configuration_list = NULL;

/// Master configuration data
naga_data* naga_cfg = NULL;

/// Index of current key being manipulated
int current_key = -1;

/// Index of keystroke being manipulated
int current_keystroke = -1;

/// Default names of key mappings for new configs
static const char *default_key_names[] = {
    "Button 01",
    "Button 02",
    "Button 03",
    "Button 04",
    "Button 05",
    "Button 06",
    "Button 07",
    "Button 08",
    "Button 09",
    "Button 10",
    "Button 11",
    "Button 12",
};

/**
 * Clear out a configuration, set names to defaults, etc.
 * @param cfg Pointer to the configuration to initialize
 * @param othercfg Index of configuration to clone, or > naga_cfg->num_configs for none
 **/
void initialize_new_config(naga_config_data* cfg, int othercfg)
{
    naga_config_data* p = NULL;
    int n, s;

    if(othercfg < (naga_cfg->num_configs - 1) && othercfg >= 0) {
        p = &naga_cfg->configs[othercfg];

        memcpy(cfg, p, sizeof(naga_config_data));

        /* Now fix the strings */
        cfg->name = strdup(cfg->name);
	for(n = 0; n < MAX_KEYS; n++) {
	    cfg->keys[n].name = strdup(cfg->keys[n].name);
	    for(s = 0; s < cfg->keys[n].key_count; s++) {
		cfg->keys[n].data[s].display = strdup(cfg->keys[n].data[s].display);
	    }
	}
    } else {
        memset(cfg, 0, sizeof(cfg));

	for(n = 0; n < MAX_KEYS; n++) {
	    cfg->keys[n].name = strdup(default_key_names[n]);
	}
    }
}

/**
 *
 **/
static void populate_configuration_list()
{
    int n;
    Fl_Menu_Item tmp = { 0 };


    if(configuration_list) {
        configuration->menu(&tmp);
        delete [] configuration_list;
        configuration_list = NULL;
    }

    configuration_list = new Fl_Menu_Item[naga_cfg->num_configs + 1];
    memset(configuration_list, 0, sizeof(Fl_Menu_Item) * (naga_cfg->num_configs + 1));
    for(n = 0; n < naga_cfg->num_configs; n++) {
        configuration_list[n].label(naga_cfg->configs[n].name);
    } 
    configuration->menu(configuration_list);
    if(naga_cfg->current_config >= 0) {
        configuration->value(naga_cfg->current_config);
    } else {
        configuration->value(0);
    }
    configuration->redraw();
}

/**
 * Add a blank configuration with the given name.
 * @param newtxt Name for new configuration
 * @param model Model: SWTOR, classic, etc
 * @param othercfg Index of configuration to clone into this one, 
 *                 or -1 if this is a new blank configuration
 **/
void add_new_configuration(const char* newtxt, model_type model, int othercfg)
{
    Fl_Menu_Item* old = configuration_list;
    char* txt = strdup(newtxt);

    ++naga_cfg->num_configs;

    /* Open up a new spot in the configuration data */
    naga_config_data* olddata = naga_cfg->configs;
    naga_cfg->configs = new naga_config_data[naga_cfg->num_configs];
    memset(naga_cfg->configs, 0, sizeof(naga_config_data) * naga_cfg->num_configs);
    memcpy(naga_cfg->configs, olddata, sizeof(naga_config_data) * (naga_cfg->num_configs - 1));
    delete [] olddata;
    initialize_new_config(naga_cfg->configs + (naga_cfg->num_configs - 1), othercfg);
    naga_cfg->configs[naga_cfg->num_configs-1].name = txt;
    naga_cfg->configs[naga_cfg->num_configs-1].model = model;
    set_current_configuration(naga_cfg->num_configs-1);

    populate_configuration_list();
}

/**
 * Starting up the app, invoked from main.  Load everything
 * we need and get ready to roll.
 **/
void startup()
{
    /* Load our previous settings */
    char fname[PATH_MAX];
    struct passwd* pw = getpwuid(getuid());
    int n;
    snprintf(fname, sizeof(fname), "%s/%s", (pw ? pw->pw_dir : "."), CFG_FILE_NAME);
    naga_cfg = load_configs(fname);

    populate_configuration_list();
    set_current_configuration(naga_cfg->current_config);
}

/**
 * App is shutting down, save the configs and kill off the main window.
 **/
void shutdown()
{
    /* Cheap and sleazy - tell any naga to reload */
    system("killall -HUP naga_daemon");
    delete main_window;
}

/**
 * Set the current key, by index into global arrays.
 * Pass in -1 to clear the current selection to nothing.
 **/
void set_current_key(int key)
{
    int n;

    if(current_key >= 0) {
        key_buttons[current_key]->clear();
    }

    current_key = key;
    if(current_key >= 0) {
        key_buttons[key]->set();

        /* List the key(s) mapped in the browser */
        key_browser->clear();
        if(naga_cfg->configs[naga_cfg->current_config].keys[key].key_count > 0) {
            for(n = 0; n < naga_cfg->configs[naga_cfg->current_config].keys[key].key_count; n++) {
                key_browser->add(naga_cfg->configs[naga_cfg->current_config].keys[key].data[n].display);
            }
        }

        /* Activate the proper controls */
        if(naga_cfg->configs[naga_cfg->current_config].keys[key].type == SINGLE_KEY ||
           naga_cfg->configs[naga_cfg->current_config].keys[key].type == MULTI_KEY) {
            set_key_mapping_button->activate();
        } else {
            set_key_mapping_button->deactivate();
        }
        key_remote_check_button->value(naga_cfg->configs[naga_cfg->current_config].keys[key].remote);
        key_remote_check_button->activate();

        key_mapping_name_input->value(naga_cfg->configs[naga_cfg->current_config].keys[key].name);
        key_mapping_name_input->activate();
        key_mapping_type_choice->value((int)naga_cfg->configs[naga_cfg->current_config].keys[key].type);
        key_mapping_type_choice->activate();

        /* Set our repeat button/delay input properly */
        if(naga_cfg->configs[naga_cfg->current_config].keys[key].type == MULTI_KEY) {
            key_repeat_check_button->activate();
            key_repeat_check_button->value(naga_cfg->configs[naga_cfg->current_config].keys[key].repeat);
        } else {
            key_repeat_check_button->deactivate();
        }

        if(key_repeat_check_button->active() && key_repeat_check_button->value()) {
            key_repeat_delay_input->activate();
            key_repeat_delay_input->value(naga_cfg->configs[naga_cfg->current_config].keys[key].repeat_delay);
        } else {
            key_repeat_delay_input->deactivate();
        }
    } else {
      /* No key map active, clear all relevant controls */
        key_browser->clear();
        set_key_mapping_button->deactivate();
        key_mapping_name_input->value("");
        key_mapping_name_input->deactivate();
        key_mapping_type_choice->deactivate();
        key_repeat_check_button->deactivate();
        key_repeat_delay_input->deactivate();
        key_remote_check_button->deactivate();
    }
}

/**
 * Select a different configuration to edit.
 * Activates or hides the buttons not available on the
 * current device
 **/
void set_current_configuration(int cfg)
{
    int n;
    int max = MAX_KEYS;

    set_current_key(-1);
    naga_cfg->current_config = cfg;

    for(n = 0; n < MAX_KEYS; n++) {
        if(cfg >= 0 && n < max) {
            key_buttons[n]->activate();
        } else {
            key_buttons[n]->deactivate();
        }
    }
}

/**
 * Handle changes to the key repeat delay input
 **/
void change_key_repeat_delay(int value, int cfg, int key)
{
    naga_cfg->configs[cfg].keys[key].repeat_delay = value;
}

/**
 * Handle changes to the key repeat check button.
 **/
void change_key_repeat_flag(int value, int cfg, int key)
{
    naga_cfg->configs[cfg].keys[key].repeat = value;
}

/**
 * Handle changes to the key repeat check button.
 **/
void change_key_remote_flag(int value, int cfg, int key)
{
    naga_cfg->configs[cfg].keys[key].remote = value;
}

/**
 * Handle changes to the key mapping name input field.
 **/
void change_key_mapping_name(const char* txt, int cfg, int key)
{
    if(naga_cfg->configs[cfg].keys[key].name) {
        free(naga_cfg->configs[cfg].keys[key].name);
    }
    naga_cfg->configs[cfg].keys[key].name = strdup(txt);
}

/**
 * Invoked by changing the key mapping type dropdown.
 **/
void set_current_key_mapping_type(int type)
{
    if(naga_cfg->current_config >= 0 && current_key >= 0) {
        naga_cfg->configs[naga_cfg->current_config].keys[current_key].type = (key_map_type)type;
    }
    if(naga_cfg->configs[naga_cfg->current_config].keys[current_key].type == SINGLE_KEY ||
       naga_cfg->configs[naga_cfg->current_config].keys[current_key].type == MULTI_KEY) {
        set_key_mapping_button->activate();
    } else {
        set_key_mapping_button->deactivate();
    }
    /* Enable key repeat check button and delay input if this is a 'keys in sequence' */
    if(naga_cfg->configs[naga_cfg->current_config].keys[current_key].type == MULTI_KEY) {
        key_repeat_check_button->activate();
        key_repeat_check_button->value(naga_cfg->configs[naga_cfg->current_config].keys[current_key].repeat);
    } else {
        key_repeat_check_button->deactivate();
    }
    if(key_repeat_check_button->active() && key_repeat_check_button->value()) {
        key_repeat_delay_input->activate();
        key_repeat_delay_input->value(naga_cfg->configs[naga_cfg->current_config].keys[current_key].repeat_delay);
    } else {
        key_repeat_delay_input->deactivate();
    }
}

/**
 * Change the name of the current configuration to the given text.
 **/
void rename_current_configuration_done(const char* txt)
{
    if(naga_cfg->current_config >= 0) {
        free(naga_cfg->configs[naga_cfg->current_config].name);
        naga_cfg->configs[naga_cfg->current_config].name = strdup(txt);
    }
    configuration_list[naga_cfg->current_config].label(naga_cfg->configs[naga_cfg->current_config].name);
    configuration->redraw();
}

/**
 * Remove the current configuration 
 **/
void delete_current_configuration_done()
{
    int k;
    
    if(naga_cfg->current_config >= 0) {
        /* Free up all of the name strings */
        free(naga_cfg->configs[naga_cfg->current_config].name);
	for(k = 0; k < MAX_KEYS; k++) {
	    if(naga_cfg->configs[naga_cfg->current_config].keys[k].name) {
		free(naga_cfg->configs[naga_cfg->current_config].keys[k].name);
	    }
	}

        /* If it's not the last config, compress the array down one */
        if(naga_cfg->current_config < naga_cfg->num_configs - 1) {
            memcpy(&naga_cfg->configs[naga_cfg->current_config], 
                   &naga_cfg->configs[naga_cfg->current_config + 1],
                   sizeof(naga_config_data) * (naga_cfg->num_configs - naga_cfg->current_config - 1));
        }

        naga_cfg->num_configs--;

        set_current_configuration(naga_cfg->num_configs - 1);

        populate_configuration_list();
    }
}

#define NSHIFTKEY   "Shift+"
#define NCONTROLKEY "Ctl+"
#define NALTKEY     "Alt+"
#define NSUPERKEY   "Super+"
#define MAX_KEYDSP_LEN 128

/**
 * Takes in a key event, and spits out a proper string
 * representing that key.  The key state is rendered
 * along the lines:
 * Ctl+Alt+Shift+F1
 **/
const char* create_key_display(const XKeyEvent* xkey, int delay)
{
  int len = 1;
  const char* xdsp;
  static char dsp[MAX_KEYDSP_LEN];

  if(xkey->state & (ShiftMask | LockMask)) {
    len += strlen(NSHIFTKEY);
  }
  if(xkey->state & ControlMask) {
    len += strlen(NCONTROLKEY);
  }
  if(xkey->state & Mod1Mask) {
    len += strlen(NALTKEY);
  }
  if(xkey->state & Mod4Mask) {
    len += strlen(NSUPERKEY);
  }
  xdsp = XKeysymToString(XLookupKeysym(const_cast<XKeyEvent*>(xkey), xkey->state & ShiftMask));
  len += strlen(xdsp);
  snprintf(dsp, MAX_KEYDSP_LEN, "%s%s%s%s%s\t%dms",
   (xkey->state & (ShiftMask | LockMask)) ? NSHIFTKEY : "", 
   (xkey->state & ControlMask) ? NCONTROLKEY : "",
   (xkey->state & Mod1Mask) ? NALTKEY : "",
   (xkey->state & Mod4Mask) ? NSUPERKEY : "",
   xdsp, delay);

  return dsp;
}

/**
 * Apply button hit, save the config and signal the daemon.
 **/
void save()
{
    char fname[PATH_MAX + 1];
    struct passwd* pw = getpwuid(getuid());

    snprintf(fname, sizeof(fname), "%s/%s", (pw ? pw->pw_dir : "."), CFG_FILE_NAME);

    save_configs(fname, naga_cfg);
}

