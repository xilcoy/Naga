/*
    Naga configuration tools to support Razer's Naga Mouse

    Copyright (C) 2015 Coy Barnes

    This code is very heavily derived from the Nostromo_n50 configuration tools
    for Belkin's Nostromo n50
    Copyright (C) 2003 Paul Bohme and others

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef NAGA_DATA_H
#define NAGA_DATA_H

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/time.h>

#define CFG_FILE_NAME ".nagarc"

//! The number of keys on the Naga device
#define MAX_KEYS 12

//! How many keystrokes per key to map (for convenience)
#define MAX_KEYSTROKES 32

/**
 * Key mapping types, sets the behavior of the key 
 * when it's hit.
 **/
typedef enum
{
    SINGLE_KEY,     /**< Maps 1:1 naga->keyboard key */
    MULTI_KEY,      /**< Map multiple keys per naga button */
    SHIFT_KEY,      /**< Hold Shift while key is held */
    CONTROL_KEY,    /**< Hold Control while key is held */
    ALT_KEY,        /**< Hold Alt while key is held */
    SUPER_KEY,      /**< Hold Super while key is held */
} key_map_type;

/**
 * Model designations.
 **/
typedef enum {
    CLASSIC,
    EPIC,
    SWTOR_MMO,
    V2012,
    HEX,
    V2014
} model_type;

/**
 * Key modes for the daemon.
 **/
typedef enum {
    NULL_MODE = -1,     /**< NULL state */
    NORMAL_MODE = 0,    /**< No lights on */
    BLUE_MODE,          /**< Blue light on */
    GREEN_MODE,         /**< Green light on */
    RED_MODE            /**< Red light on */
} mode_type;

/**
 * Whether a key_stroke_data describes a keystroke or a mouse
 * button.
 **/
typedef enum {
    STROKE_KEY = 0, 
    STROKE_MOUSE
} key_stroke_type;

/**
 * One keystroke in a series that gets mapped to a single naga key.
 **/
typedef struct
{
    key_stroke_type type; /**< Keystroke or mouse hit? */
    int code;             /**< Raw scan code of the key */
    int state;            /**< State flags */
    char* display;        /**< Display string for this keystroke */
    int delay;            /**< Delay until next keystroke */
} naga_key_stroke_data;

/**
 * Info for a single naga key
 **/
typedef struct 
{
    char* name;         /**< Display name */
    key_map_type type;  /**< What the key does when hit */
    short key_count;    /**< Number of keystrokes mapped */
    naga_key_stroke_data data[MAX_KEYSTROKES]; /**< Keystrokes mapped */
    int repeat;         /**< Whether to repeat when key is held down */
    int repeat_delay;   /**< Amount of time to delay between keystrokes. */
    int remote;         /**< Whether to ship this to the remote node or not */

    int pressed;        /**< Used by daemon to eat extra events when n52 keys held */
} naga_key_config_data;

/**
 * One set of key mappings.
 **/
typedef struct 
{
  char* name;                                     /**< Display name */
  model_type model;                               /**< Device model */
  naga_key_config_data keys[MAX_KEYS];            /**< keystrokes for each key */
} naga_config_data;

typedef struct
{
  int network_enabled;
  int port;
  char* server;
  int num_configs;
  int current_config;
  naga_config_data* configs;
} naga_data;

void save_configs(const char* fname, const naga_data* data);
naga_data* load_configs(const char* fname);

#ifdef __cplusplus
}
#endif

#endif // NAGA_DATA_H
